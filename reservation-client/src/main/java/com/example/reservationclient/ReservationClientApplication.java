package com.example.reservationclient;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@EnableFeignClients

@SpringBootApplication
public class ReservationClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReservationClientApplication.class, args);
    }
}


@Component
class MyCLR implements CommandLineRunner {


    @Autowired
    ReservationReader repo;

    @Override
    public void run(String... args) throws Exception {

        System.out.println("Executed");

        while (true) {
            Thread.sleep(1000);
            repo.bookings()
                    .getContent()
                    .stream()
                    .map(Booking::getReference)
                    .forEach(System.out::println);
        }


    }
}
