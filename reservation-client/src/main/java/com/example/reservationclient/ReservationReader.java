package com.example.reservationclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("reservation-service")

public interface ReservationReader {

    @RequestMapping(method = RequestMethod.GET, value = "/bookings")
    Resources<Booking> bookings();



}